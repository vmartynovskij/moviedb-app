//
//  MovieListTableViewController.swift
//  MovieDb
//
//  Created by Viktor on 12/9/19.
//  Copyright © 2019 FaceIT. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire

class MovieListTableViewController: UITableViewController, UISearchResultsUpdating {
    
    var page: Int = 1
    var totalPages: Int = Int.max
    
    var searchPage: Int = 1
    var totalSearchPages: Int = Int.max
    
    var movies:[MovieModel] = []
    var searchMovies:[MovieModel] = []
    
    var isSearch:Bool = false
    var searchQuery: String = ""
    
    var offlineMessageShowed = false
    
    func showMessage(title: String? = "", message: String? = ""){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            alertController.dismiss(animated: true, completion: nil)
        }))
        self.present(alertController, animated: true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.register(UINib(nibName: "MovieTableViewCell", bundle: nil), forCellReuseIdentifier: "MovieTableViewCell")
        
        Logic.shared.favs = UserDefaults.standard.array(forKey: "favs")  as? [Int] ?? [Int]()
        
        movies = []
        loadMovies()
        
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search"
        self.navigationItem.searchController = searchController
        self.definesPresentationContext = true

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tableView.beginUpdates()
        self.tableView.reloadRows(at: self.tableView!.indexPathsForVisibleRows!, with: UITableView.RowAnimation.fade)
        self.tableView.endUpdates()
    }

    
    func updateSearchResults(for searchController: UISearchController) {
        
        searchQuery = (searchController.searchBar.text ?? "")
        print("Searching with: " + searchQuery)
        
        if (searchQuery.count > 0){
            isSearch = true
            searchMovies = []
            searchPage = 1
            searchMovies(query: searchQuery)
        } else {
           cancelSearch()
        }
    }
    
    func cancelSearch(){
        isSearch = false
        searchPage = 1
        searchQuery = ""
        self.tableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        cancelSearch()
    }

    func searchMovies(query:String = ""){
        if (searchPage < totalSearchPages && Connectivity.isConnectedToInternet){
            SVProgressHUD.show()
            NetworkService.init().searchMovieList(page: searchPage, query: query, completion: { (json, isCompleted) in
                SVProgressHUD.dismiss()
                
                if (isCompleted){
                    print(json)
                    
                    let newMovies = MovieModel.array(jsons: json["results"].arrayValue)
                    self.searchMovies.append(contentsOf: newMovies)
                    self.searchPage+=1
                    self.totalSearchPages = json["total_pages"].intValue
                    
                    self.tableView.reloadData()
                } else {
                    self.showMessage(title: "Error", message: "")
                }
            })
        }
    }
    
    func loadMovies(){
                    
            if (page < totalPages && Connectivity.isConnectedToInternet){
            SVProgressHUD.show()
            NetworkService.init().getMovieList(page: page, completion: { (json, isCompleted) in
                SVProgressHUD.dismiss()
                
                if (isCompleted){
                    print(json)
                    
                    self.offlineMessageShowed = false
                    
                    let newMovies = MovieModel.array(jsons: json["results"].arrayValue)
                    self.movies.append(contentsOf: newMovies)
                    self.page+=1
                    self.totalPages = json["total_pages"].intValue
                    
                    self.tableView.reloadData()
                    
                    if (Logic.shared.savedMovies.count < self.movies.count){
                        do {
                            let encodedData: Data = try NSKeyedArchiver.archivedData(withRootObject: self.movies, requiringSecureCoding: false)
                            UserDefaults.standard.set(encodedData, forKey: "saved_movies")
                            UserDefaults.standard.set(self.page, forKey: "lastPage")
                            UserDefaults.standard.synchronize()
                        } catch{
                            print ("Error saving movies")
                        }
                        

                    }
                } else {
                    self.showMessage(title: "Error", message: "")
                }
            })
            } else {
                if (self.movies.count == 0){
                    self.page = UserDefaults.standard.integer(forKey: "lastPage")
                    let decoded  = UserDefaults.standard.data(forKey: "saved_movies")
                    self.movies = NSKeyedUnarchiver.unarchiveObject(with: decoded!) as! [MovieModel]
                    self.tableView.reloadData()
                    
                } else {
                    if (offlineMessageShowed == false){
                        offlineMessageShowed = true
                        self.showMessage(title: "Error", message: "Please check your internet connection")
                    }
                }
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return isSearch ? self.searchMovies.count : self.movies.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MovieTableViewCell", for: indexPath) as! MovieTableViewCell

        let movie = isSearch ? self.searchMovies[indexPath.row] : self.movies[indexPath.row]
        
        cell.movieTitleLabel?.text = movie.title
        cell.favBtn.isHidden = !Logic.shared.favs.contains(movie.id)
        
        
        if indexPath.row == (isSearch ? self.searchMovies.count : self.movies.count) - 1 {
            
            if (isSearch){
                if (self.searchMovies.count > 19){
                    self.searchMovies(query: searchQuery)
                }
            } else {
                self.loadMovies()
            }
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false);
        let storyboard = UIStoryboard(name: "Main", bundle: nil);
        let movieVC = storyboard.instantiateViewController(withIdentifier: "movieVC") as! MovieViewController
        movieVC.movie = isSearch ? self.searchMovies[indexPath.row] : self.movies[indexPath.row]
        self.navigationController?.pushViewController(movieVC, animated: true)
    }

}
