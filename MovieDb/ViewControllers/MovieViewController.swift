//
//  MovieViewController.swift
//  MovieDb
//
//  Created by Viktor on 12/9/19.
//  Copyright © 2019 FaceIT. All rights reserved.
//

import UIKit
import Kingfisher

class MovieViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var releaseDateLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var favBtn: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var genresLabel: UILabel!
    @IBOutlet weak var playBtn: UIButton!
    
    var movie: MovieModel? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = movie?.title
        releaseDateLabel.text = "Release date: " + movie!.release_date
        countryLabel.text = "Original language: \(movie!.original_language)"
        overviewLabel.text = movie?.overview

        imgView.kf.setImage(with: URL(string: movie!.posterUrl), placeholder: UIImage(named: "poster-placeholder"))

        let font = UIFont(name: "Helvetica", size: 17.0)
        let height = heightForView(text: movie!.overview, font: font!, width: overviewLabel.frame.width)
        overviewLabel.frame.size.height = height + 40
        
        if (Logic.shared.favs.contains(movie!.id)){
            favBtn.setImage(UIImage.init(named: "fav_icon_on"), for: UIControl.State.normal)
        }
        ratingLabel.text = String.init(format: "Average score: %.2f", movie!.vote_average)
        
//        playBtn.isHidden = !movie!.video
        playBtn.isHidden = true
        
        if (movie!.genre_ids.count > 0 && Logic.shared.genres.count > 0){
            genresLabel.text = "Genres: "
            for genre in movie!.genre_ids{
                //            genresLabel.text = genresLabel.text! + "\(genre)" + ", "
                let genreString = Logic.shared.genres.filter { (genreModel) -> Bool in
                    return genreModel.id == genre
                }.first!.name
                genresLabel.text = genresLabel.text! + genreString + ", "
            }
            
            genresLabel.text?.removeLast(2)
        } else {
            genresLabel.isHidden = true
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        scrollView.contentSize = CGSize(width: self.view.frame.width, height: overviewLabel.frame.origin.y + overviewLabel.frame.size.height + 20)
    }
    
     func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text

        label.sizeToFit()
        return label.frame.height
    }


    @IBAction func favBtnAction(_ sender: UIButton) {
        
        if (Logic.shared.favs.contains(movie!.id)){
            Logic.shared.favs.removeAll { (item) -> Bool in
                return item == movie!.id
            }
            
            favBtn.setImage(UIImage.init(named: "fav_icon"), for: UIControl.State.normal)
        } else {
            Logic.shared.favs.append(movie!.id)
            favBtn.setImage(UIImage.init(named: "fav_icon_on"), for: UIControl.State.normal)
        }
        
        UserDefaults.standard.set(Logic.shared.favs, forKey: "favs")

    }
    
    @IBAction func playBtnAction(_ sender: UIButton) {
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
