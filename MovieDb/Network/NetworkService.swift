//
//  NetworkService.swift
//  MovieDb
//
//  Created by Viktor on 12/9/19.
//  Copyright © 2019 FaceIT. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class NetworkService: NSObject {
    let API = "https://api.themoviedb.org/3"
    let API_KEY = "1f2111c19a701dda1f631d4d00b699f7"
    let IMG_PATH = "https://image.tmdb.org/t/p/w600_and_h900_bestv2"
    
    func getMovieList(page:Int = 0, completion: @escaping (JSON, Bool)->Void){
        
        var url = API + "/movie/now_playing?api_key=\(API_KEY)"
        if (page > 0){
            url = url + "&page=\(page)"
        }
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseData{ (response) in
            self.complete(response: response, completion: { (json, isComplete) in
                completion(json, isComplete)
            })
        }
    }
    
    func searchMovieList(page:Int = 0, query:String, completion: @escaping (JSON, Bool)->Void){
        
        var url = API + "/search/movie?api_key=\(API_KEY)&query=\(query)"
        if (page > 0){
            url = url + "&page=\(page)"
        }
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseData{ (response) in
            self.complete(response: response, completion: { (json, isComplete) in
                completion(json, isComplete)
            })
        }
    }
    
    func getGenres(completion: @escaping (JSON, Bool)->Void){
        
        let url = API + "/genre/movie/list?api_key=\(API_KEY)"
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseData{ (response) in
            self.complete(response: response, completion: { (json, isComplete) in
                completion(json, isComplete)
            })
        }
    }
    
    func complete (responseJSON : DataResponse<Any>, completion: @escaping (JSON, Bool)->Void)
    {
        guard responseJSON.result.isSuccess else {
            print("Error while fetching data: \(String(describing: responseJSON.result.error))")
            completion(JSON.null, false)
            return
        }
        
        if let result = responseJSON.result.value {
            let serverResponse = result
            print(responseJSON.request!.httpMethod! + " : " + responseJSON.request!.url!.absoluteString)
            let data = JSON(serverResponse)
            print(data)
            if (200..<300).contains(responseJSON.response!.statusCode) {
                completion(data, true)
            } else {
                completion(data, false)
            }
        } else {
            completion([:], false)
        }
    }
    
        func complete (response : DataResponse<Data>, hideLogs:Bool? = false, completion: @escaping (JSON, Bool)->Void)
        {
            guard response.result.isSuccess else {
                print("Error while fetching data: \(String(describing: response.result.error))")
                if (response.data != nil){
                    do {
                        let json = try JSON(data: response.data!)
                        print(json)
                        completion(json, false)
                        return
                    }
                    catch {
                        completion(JSON.null, false)
                        return
                    }
                } else {
                    completion(JSON.null, false)
                    return
                }
                
            }
            
            if let result = response.result.value {
                let serverResponse = result
    //            print("******************** delete Image **********************")
                print(response.request!.httpMethod! + " : " + response.request!.url!.absoluteString)
                let data = JSON(serverResponse)
                if (response.request!.url!.absoluteString.contains("v1/get-all-favorites") == false && hideLogs == false){
                    print(data)
                }
                if (200..<300).contains(response.response!.statusCode) {
                    completion(data, true)
                } else {
                    completion(data, false)
                }
            } else {
                completion([:], false)
            }
        }
}

struct Connectivity {
  static let sharedInstance = NetworkReachabilityManager()!
  static var isConnectedToInternet:Bool {
      return self.sharedInstance.isReachable
    }
}
