//
//  Logic.swift
//  MovieDb
//
//  Created by Viktor on 12/9/19.
//  Copyright © 2019 FaceIT. All rights reserved.
//

import UIKit

class Logic {
    static let shared = Logic()
    var favs:[Int] = []
    var savedMovies:[MovieModel] = []
    var genres:[GenreModel] = []
}
