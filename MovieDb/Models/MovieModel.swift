//
//  MovieModel.swift
//  MovieDb
//
//  Created by Viktor on 12/9/19.
//  Copyright © 2019 FaceIT. All rights reserved.
//

import UIKit
import SwiftyJSON

class MovieModel: OriginModel, NSCoding {
    required init?(coder: NSCoder) {
        super.init()
        
        self.title = coder.decodeObject(forKey: "title") as! String
        self.overview = coder.decodeObject(forKey: "overview") as! String
        self.release_date = coder.decodeObject(forKey: "release_date") as! String
        
        self.original_language = coder.decodeObject(forKey: "original_language") as! String
        self.poster_path = coder.decodeObject(forKey: "poster_path") as! String
        self.posterUrl = coder.decodeObject(forKey: "posterUrl") as! String
        
        self.popularity = coder.decodeDouble(forKey: "popularity")
        self.id = coder.decodeInteger(forKey: "id")
        
        self.genre_ids = coder.decodeObject(forKey: "genre_ids") as! [Int]
        self.vote_average = coder.decodeDouble(forKey: "vote_average")
        self.vote_count = coder.decodeInteger(forKey: "vote_count")
        self.video = coder.decodeBool(forKey: "video")
    }
    
    
    func encode(with coder: NSCoder) {
        coder.encode(title, forKey: "title")
        coder.encode(overview, forKey: "overview")
        coder.encode(release_date, forKey: "release_date")
        
        coder.encode(original_language, forKey: "original_language")
        coder.encode(poster_path, forKey: "poster_path")
        coder.encode(posterUrl, forKey: "posterUrl")
        
        coder.encode(popularity, forKey: "popularity")
        coder.encode(id, forKey: "id")
        
        coder.encode(genre_ids, forKey: "genre_ids")
        coder.encode(vote_average, forKey: "vote_average")
        coder.encode(vote_count, forKey: "vote_count")
        coder.encode(video, forKey: "video")
    }

    var title = ""
    var overview = ""
    var release_date = ""
    var original_language = ""
    var poster_path = ""
    var posterUrl = ""
    
    var genre_ids: [Int] = []
    var vote_average = 0.0
    var vote_count = 0
    var video = false
    
    var popularity = 0.0
    var id = -1
    
        override init(json: JSON){
            super.init(json: json)

            self.title = json["title"].string ?? ""
            self.overview = json["overview"].string ?? ""
            self.release_date = json["release_date"].string ?? ""
            self.original_language = json["original_language"].string ?? ""
            self.poster_path = json["poster_path"].string ?? ""
            self.posterUrl = "https://image.tmdb.org/t/p/w220_and_h330_face" + self.poster_path
            
            self.popularity = json["popularity"].double ?? 0
            self.id = json["id"].int ?? -1
            
            self.genre_ids = (json["genre_ids"].arrayObject as! [Int])
            self.vote_average = json["vote_average"].double ?? 0
            self.video = json["video"].boolValue

        }
        
        static func array(jsons: [JSON]) -> [MovieModel]{
            var array = [MovieModel]()
            for json in jsons{
                array.append(MovieModel(json: json))
            }
            return array
        }
}

class GenreModel: OriginModel, NSCoding {
    
    var name = ""
    var id = -1
    
    required init?(coder: NSCoder) {
        super.init()
        
        self.name = coder.decodeObject(forKey: "name") as! String
        self.id = coder.decodeInteger(forKey: "id")
    }
    
    
    func encode(with coder: NSCoder) {
        coder.encode(name, forKey: "name")
        coder.encode(id, forKey: "id")
    }
    
    override init(json: JSON){
        super.init(json: json)

        self.name = json["name"].string ?? ""
        self.id = json["id"].int ?? -1

    }
    
    static func array(jsons: [JSON]) -> [GenreModel]{
            var array = [GenreModel]()
            for json in jsons{
                array.append(GenreModel(json: json))
            }
            return array
        }

}
