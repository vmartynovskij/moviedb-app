//
//  OriginModel.swift
//  MovieDb
//
//  Created by Viktor on 12/9/19.
//  Copyright © 2019 FaceIT. All rights reserved.
//

import UIKit
import SwiftyJSON

class OriginModel: NSObject {
    override init() {}
    init(json: JSON) {}
    
    static func jsonToArray(string:String) -> [Any] {
        do {
            if let jsonArray = try JSONSerialization.jsonObject(with: string.data(using: .utf8)!, options : .allowFragments) as? [Any] 
            {
                return jsonArray
            } else {
                print("bad json")
                  return[]
            }
        } catch let error as NSError {
            print(error)
            return[]
        }
    }
}
