//
//  AppDelegate.swift
//  MovieDb
//
//  Created by Viktor on 12/9/19.
//  Copyright © 2019 FaceIT. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?



    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        let decoded  = UserDefaults.standard.data(forKey: "saved_movies")
        if (decoded != nil){
            Logic.shared.savedMovies = NSKeyedUnarchiver.unarchiveObject(with: decoded!) as! [MovieModel]
        }
        
        NetworkService.init().getGenres { (json, isComplete) in
            if (isComplete){
                print (json)
                Logic.shared.genres = GenreModel.array(jsons: json["genres"].arrayValue)
                
    //save to USerDefaults (do it later)
                
//                do {
//                    let encodedData: Data = try NSKeyedArchiver.archivedData(withRootObject: Logic.shared.genres, requiringSecureCoding: false)
//                    UserDefaults.standard.set(encodedData, forKey: "genres")
//                    UserDefaults.standard.synchronize()
//                } catch{
//                    print ("Error saving genres")
//                }
            }
        }

        
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

